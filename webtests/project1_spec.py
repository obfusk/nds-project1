import httpony.client as C
import os
import threading
import time
import unittest

port = 8000

class Test(unittest.TestCase):

  def uri(self, path = ""):
    return "localhost:{}/{}".format(port, path)

  def test_1_single_resource(self):
    c = C.Client(); r = c.get(self.uri("index.html"))
    self.assertEqual(r.status, 200)
    self.assertRegexpMatches(r.force_body, 'src="img/2.png"')

  def test_2_nonexistent(self):
    c = C.Client(); r = c.get(self.uri("nonexistent.html"))
    self.assertEqual(r.status, 404)
    self.assertEqual(r.force_body, "")

  def test_3_cached(self):
    c = C.Client()

    r1 = c.get(self.uri("img/1.png"))
    self.assertEqual(r1.status, 200)
    self.assertEqual(len(r1.force_body), 448)

    r2 = c.get(self.uri("img/1.png"),
               headers = { "If-None-Match": r1.headers["ETag"] } )
    self.assertEqual(r2.status, 304)
    self.assertEqual(r2.force_body, "")

  def test_4_index(self):
    c = C.Client(); r = c.get(self.uri())
    self.assertEqual(r.status, 200)
    with open("content/index.html") as f:
      index = f.read()
    self.assertEqual(r.force_body, index)

  def test_5_no_index(self):
    c = C.Client(); r = c.get(self.uri("img/"))
    self.assertEqual(r.status, 200)
    self.assertRegexpMatches(r.force_body, 'href="2.png"')

  def test_6_multiple_gets_with_close(self):
    c = C.Client(persistent = True)

    r1 = c.get(self.uri("img/1.png"))
    self.assertEqual(r1.status, 200)
    self.assertEqual(len(r1.force_body), 448)

    r2 = c.get(self.uri("img/2.png"))
    self.assertEqual(r2.status, 200)
    self.assertEqual(len(r2.force_body), 1046)

    r3 = c.get(self.uri("img/3.png"),
               headers = { "Connection": "close" })
    self.assertEqual(r3.status, 200)
    self.assertEqual(len(r3.force_body), 1240)

    # confirm that the connection was closed
    c.max_tries = 1
    with self.assertRaisesRegexp(C.Error, "max retries"):
      c.get(self.uri("does-not-matter"))

  def test_7_multiple_gets_with_timeout(self):
    c = C.Client(persistent = True)

    r1 = c.get(self.uri("img/1.png"))
    self.assertEqual(r1.status, 200)
    self.assertEqual(len(r1.force_body), 448)

    r2 = c.get(self.uri("img/2.png"))
    self.assertEqual(r2.status, 200)
    self.assertEqual(len(r2.force_body), 1046)

    r3 = c.get(self.uri("img/3.png"))
    self.assertEqual(r3.status, 200)
    self.assertEqual(len(r3.force_body), 1240)

    time.sleep(2) # timeout is 1

    # confirm that the connection was closed
    c.max_tries = 1
    with self.assertRaisesRegexp(C.Error, "max retries"):
      c.get(self.uri("does-not-matter"))

  def test_8_parallel_gets(self):
    responses = []

    def thread1():
      c  = C.Client(persistent = True)
      r1 = c.get(self.uri("img/1.png"))
      responses.append(r1)
      r2 = c.get(self.uri("img/2.png"))
      responses.append(r2)

    def thread2():
      c  = C.Client(persistent = True)
      r3 = c.get(self.uri("img/3.png"))
      responses.append(r3)
      time.sleep(1) # make index last
      r4 = c.get(self.uri("index.html"))
      responses.append(r4)

    t1 = threading.Thread(target = thread1)
    t2 = threading.Thread(target = thread2)

    t1.start(); t2.start(); t1.join(); t2.join()

    for r in responses:
      self.assertEqual(r.status, 200)

    lens = [ len(r.force_body) for r in responses ]

    self.assertEqual(sorted(lens), [416, 448, 1046, 1240])
    self.assertGreater(lens.index(1046), lens.index(448)) # in-order
    self.assertEqual(lens[-1], 416)                       # index last

if __name__ == "__main__":
  port = int(os.environ.get("PORT", "") or 0) or port
  unittest.main()

# vim: set tw=70 sw=2 sts=2 et fdm=marker :
