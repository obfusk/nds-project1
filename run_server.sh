#!/bin/bash
export PYTHONPATH="$PWD/webserver/lib"
python -m httpony.server path="content" port="${1:-8000}" timeout=1
